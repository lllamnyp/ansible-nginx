#!/bin/bash
docker exec -t ansible ansible-playbook -i /task/prod /task/01-install_nginx.yml
docker exec -t ansible ansible-playbook -i /task/prod /task/02-fetch_latest_config.yml
echo
echo Testing webserver
curl localhost:51080/example/index.html
