#!/bin/bash
upstream_host=$(docker run --name upstream --network lab-net -d \
  -v `pwd`/upstream/nginx.conf:/etc/nginx/nginx.conf \
  -v `pwd`/upstream/app.conf:/etc/nginx/conf.d/default.conf \
  -v `pwd`/upstream/static.html:/var/www/example/index.html \
  nginx)
echo $upstream_host > upstream_host
