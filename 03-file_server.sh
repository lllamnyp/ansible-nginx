#!/bin/bash
tar -C ./config -cf fileserver/config.tar location.conf upstream.conf
fileserver=$(docker run --name fileserver --network lab-net -d \
  -v `pwd`/fileserver/nginx.conf:/etc/nginx/nginx.conf \
  -v `pwd`/fileserver/app.conf:/etc/nginx/conf.d/default.conf \
  -v `pwd`/fileserver/config.tar:/var/www/config.tar \
  nginx)
echo $fileserver > fileserver_host
