#!/bin/bash
mkdir -p tmp
base64 /dev/urandom | head > tmp/location.conf
base64 /dev/urandom | head > tmp/upstream.conf
tar -C ./tmp -cf fileserver/config.tar location.conf upstream.conf
echo Attempting to break nginx
echo
docker exec -t ansible ansible-playbook -i /task/prod /task/02-fetch_latest_config.yml
echo
echo Testing web server
curl localhost:51080/example/index.html
