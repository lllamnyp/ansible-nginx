Запускать скрипты по порядку:

```
./00-create_host.sh 
./01-ansible.sh 
./02-upstream_server.sh 
./03-file_server.sh 
./04-run_playbooks.sh 
./05-generate_random_bad_config.sh 
./06-ensure_correct_config.sh 
```

С 0 по 3 накатывают тестовую среду в докерах

04 раскатывает nginx и подгружает конфиг

05 ломает конфиг и применяет роль, которая скачивает новый конфиг, затем проверяет, что конфиг не поломали

06 возвращает правильный конфиг (хотя мы его и не смогли сломать)

99 зачищает за собой всё созданное в докерах
