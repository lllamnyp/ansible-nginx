#!/bin/bash
docker rm -f $(cat centos_host)
docker rm -f $(cat upstream_host)
docker rm -f $(cat fileserver_host)
docker rm -f $(cat ansible_host)
docker network rm $(cat docker_net)
rm centos_rsa centos_rsa.pub centos_host docker_net ansible_host upstream_host fileserver_host fileserver/config.tar
ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:51022"
