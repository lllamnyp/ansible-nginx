#!/bin/bash
ssh-keygen -t rsa -N '' -f ./centos_rsa
docker_net=$(docker network create --driver bridge lab-net)
echo $docker_net > docker_net
centos_host=$(docker run -itd --name webserver --network lab-net -p 51022:22 -p 51443:443 -p 51080:80 centos:6.7)
echo $centos_host > centos_host
docker exec $centos_host useradd -m centos
docker exec $centos_host yum install sudo openssh-server openssh-clients -y
docker exec $centos_host bash -c "echo 'centos ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers"
docker cp centos_rsa.pub $centos_host:/tmp/
docker exec $centos_host mkdir -p /home/centos/.ssh
docker exec $centos_host bash -c "cat /tmp/centos_rsa.pub > /home/centos/.ssh/authorized_keys"
docker exec $centos_host chown -R centos:centos /home/centos/.ssh
docker exec $centos_host chmod 700 /home/centos/.ssh
docker exec $centos_host chmod 600 /home/centos/.ssh/authorized_keys
docker exec $centos_host service sshd restart
