#!/bin/bash
ansible_host=$(docker run --name ansible -itd -v `pwd`/ansible:/task --network lab-net williamyeh/ansible:ubuntu18.04 bash)
echo $ansible_host > ansible_host
docker exec $ansible_host mkdir -p /root/.ssh
docker cp centos_rsa $ansible_host:/root/.ssh/id_rsa
docker cp ansible_cfg/config $ansible_host:/root/.ssh/config
docker exec $ansible_host chmod 400 /root/.ssh/id_rsa
docker exec $ansible_host chmod 400 /root/.ssh/config
docker exec $ansible_host chown -R root:root /root/.ssh

